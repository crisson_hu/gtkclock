SHELL = /bin/bash
LC_ALL = C
LANG = C
LANGUAGE = C

define get_version =
$(shell sh -c "grep -w gtkclock debian/changelog" | head -1 | awk '{print $$2}' | sed 's/[()]//g')
endef

APP = gtkclock
PKG = ${APP}_$(get_version)_all.deb
PKG_FILE = ../${PKG}


install:
	mkdir -p debian/tmp/
	cp -r src/usr/ debian/tmp/

clean:
	find . -name __pycache__ -type d -exec rm -r {} \; -print -prune

checkcodestyle:
	@printf '[style-check]: start ....\n'
	@find src -name '*.py' -print | xargs pycodestyle -v
	@printf '[style-check]: done.\n'

builddebpackage:
	debuild --no-tgz-check -I -us -uc
	debclean

reinstalldeb:
	sudo dpkg -i ${PKG_FILE}

.PHONY: install clean checkcodestyle builddebpackage reinstalldeb
