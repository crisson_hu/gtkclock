![Screenshot: Gtk Clock](misc/screenshot-normal.png)


# About Gtk Clock

A simple GUI application based on pygobject to display current time in
digital clock.


# Feature

There are three mode: normal, maximized, fullscreen/screensaver.

The usage info shows more options:

    usage: gtkclock [-h] [-24] [-12] [--no-second] [-fg FGCOLOR] [-bg BGCOLOR] [-fs] [-ss] [-max]
                    [-top] [--frame {none,auto,always}] [-0] [-1] [-2] [-3] [-4] [--builtin] [-l]
                    [-lc] [-V] [-d]

    Gtk Clock

    optional arguments:
      -h, --help            show this help message and exit
      -24, --display24      Display time in 24-hour format. (default)
      -12, --display12      Display time in 12-hour format.
      --no-second           Not show second.
      -fg FGCOLOR, --foreground FGCOLOR
                            Foreground color. (default white)
      -bg BGCOLOR, --background BGCOLOR
                            Background color. (default black)
      -fs, --fullscreen     Make window fullscreen.
      -ss, --screensaver    Make window a screensaver.
      -max, --maximize      Make window maximized.
      -top, --ontop         Make window on top of others.
      --frame {none,auto,always}
                            Hide window frame when maximized. (default)
      -0, --builtin0        Builtin font #0.
      -1, --builtin1        Builtin font #1.
      -2, --builtin2        Builtin font #2.
      -3, --builtin3        Builtin font #3.
      -4, --builtin4        Builtin font #4.
      --builtin             Builtin font, same as --builtin1. (default)
      -l, --list-shortcuts  List shortcuts.
      -lc, --list-colors    List shortcuts.
      -V, --version         show program's version number and exit
      -d, --debug           Show debug output.


# Build and Install

## Build

	$ ./build.sh

## Build & Install

	$ ./build.sh -u

# TODO Items

    * Replace xscreensaver
    - start by timer

    * Replace crontab
    - scheduler
    - run paplay

    * Use standalone font
	- font should be scalable to fit window size.
