#!/bin/bash

set -e

PROJECT_DIR=$(dirname $(readlink -f $0))

function genlocale () {
    cd ${PROJECT_DIR}
    make genlocale
}

function build () {
    set -x
    cd ${PROJECT_DIR}
    make checkcodestyle
    make builddebpackage
}

function reinstalldeb () {
    set -x
    cd ${PROJECT_DIR}
    make reinstalldeb
}

printf "Project Location: \n\t'${PROJECT_DIR}'\n"

while getopts 'cu' opt; do
    printf "Input Option:\n\t'${opt}'\n"
    printf "Run:\n"
    case $opt in
        c)
            genlocale
            exit;;
        u)
            build
            reinstalldeb
            exit;;
        *)
            printf "The supported option is '-u',"
            printf " or running without any option.\n"
            exit;;
    esac
done

build
exit
