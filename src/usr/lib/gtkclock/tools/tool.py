import os
from datetime import datetime
import pprint
import traceback
import inspect
import threading


class Log:

    use_debug = False

    @classmethod
    def debug_log(cls, msg=None):
        if not cls.use_debug:
            return

        cls.print_prefix()

        if not msg:
            print()
            return

        if isinstance(msg, dict) or isinstance(msg, list):
            pprint.pprint(msg)
        else:
            try:
                print(msg)
            except Exception as e:
                print('Exception: ' + str(e))
                cls.print_prefix()
                print(str(str(msg).encode('utf-8', 'ignore')))

    @classmethod
    def todo(cls):
        i = 0
        for ln in traceback.format_stack()[:-2]:
            cls.print_prefix()
            print(' TODO: {:s}-> {:s}'.format(
                4*i*' ', ln.split('\n')[1].strip()
            ))
            i += 1

    @classmethod
    def print_prefix(cls):
        print(
            '{:s}<{:x}> [{:^15s}] '.format(
                datetime.now().strftime('%Y%m%d %H:%M:%S.%f'),
                threading.current_thread().ident,
                '{}#{}'.format(
                    inspect.stack()[2].function,
                    inspect.stack()[2].lineno,
                )
            ),
            end=''
        )
