#!/usr/bin/python3 -B

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('PangoCairo', '1.0')

if __name__ == '__main__':
    from main import Main
    main = Main()
    main.run()
