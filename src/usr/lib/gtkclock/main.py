from pathlib import Path
import argparse
import time
import threading
from datetime import datetime
from gi.repository import Gtk, Gdk, GLib, Pango, PangoCairo

from colors import Color
from tools import Log

debug_log = Log.debug_log
todo = Log.todo

appdir = Path(__file__).parent
appname = 'GTK Clock'
StartupWMClass = 'gtkclock'
StartupWMClassMax3 = 'gtkclock-max3'
StartupWMClassSS = 'gtkclock-ss'

# On button press, switch to date then back to time.
# default: 3000 milli-seconds
TOGGLE_DATE_TIME_DURATION = 3000


class Main:

    def __init__(self):
        parser = argparse.ArgumentParser(prog='gtkclock',
                                         description='Gtk Clock',
                                         allow_abbrev=False)
        parser.add_argument('-24', '--display24', action='store_true',
                            help='Display time in 24-hour format. (default)')
        parser.add_argument('-12', '--display12', action='store_true',
                            help='Display time in 12-hour format.')
        # parser.add_argument('--show-time', action='store_true',
        #                     help='Display time. (default)')
        # parser.add_argument('--show-date', action='store_true',
        #                     help='Display date.')
        parser.add_argument('--no-second', action='store_true',
                            default=False,
                            help="Not show second.")
        # parser.add_argument('--cycle', action='store_true',
        #                     help="Do color-cycling. (default)")
        # parser.add_argument('--nocycle', action='store_true',
        #                     help="Don't do color-cycling.")
        parser.add_argument('-fg', '--foreground', dest='fgcolor',
                            help="Foreground color. (default white)")
        parser.add_argument('-bg', '--background', dest='bgcolor',
                            help="Background color. (default black)")
        parser.add_argument('-fs', '--fullscreen', action='store_true',
                            help="Make window fullscreen.")
        parser.add_argument('-ss', '--screensaver', action='store_true',
                            help="Make window a screensaver.")
        parser.add_argument('-max', '--maximize', action='store_true',
                            help="Make window maximized.")
        parser.add_argument('-top', '--ontop', action='store_true',
                            help="Make window on top of others.")
        parser.add_argument('--frame', choices=['none', 'auto', 'always'],
                            default='auto',
                            help='Hide window frame when maximized. (default)')
        # parser.add_argument('--root', action='store_true',
        #                     help="Draw on the root window.")
        # parser.add_argument('-fn', '--font', dest='font',
        #                     help="Font name or builtin font.")
        parser.add_argument('-0', '--builtin0', action='store_true',
                            help="Builtin font #0.")
        parser.add_argument('-1', '--builtin1', action='store_true',
                            help="Builtin font #1.")
        parser.add_argument('-2', '--builtin2', action='store_true',
                            help="Builtin font #2.")
        parser.add_argument('-3', '--builtin3', action='store_true',
                            help="Builtin font #3.")
        parser.add_argument('-4', '--builtin4', action='store_true',
                            help="Builtin font #4.")
        parser.add_argument('--builtin', action='store_true',
                            help="Builtin font, same as --builtin1. (default)")
        # parser.add_argument('--transparent', action='store_true',
        #                     help="Make window background transparent.")
        # parser.add_argument('--nontransparent', action='store_true',
        #                     help="Make window background nontransparent.")
        parser.add_argument('-l', '--list-shortcuts', action='store_true',
                            help="List shortcuts.")
        parser.add_argument('-lc', '--list-colors', action='store_true',
                            help="List shortcuts.")
        parser.add_argument('-V', '--version', action='version',
                            version='%(prog)s 1.0')
        parser.add_argument('-d', '--debug', action='store_true',
                            help='Show debug output.')
        self.args = parser.parse_args()
        if self.args.debug:
            Log.use_debug = True
        return

    def list_shortcuts(self):
        fmt = '    %-20s%s.'
        print('Shortcuts:')
        print(fmt % ('B/b', 'toggle blank mode'))
        print(fmt % ('D/d', 'toggle window decoration'))
        print(fmt % ('Escape', 'leave fullscreen mode'))
        print(fmt % ('F/f', 'toggle between fullscreen and maximize mode'))
        print(fmt % ('S/s/space', 'toggle show second'))
        print(fmt % ('o/O', 'toggle opacity'))
        print(fmt % ('Q/q/W/w', 'close window'))
        print(fmt % ('t/T', 'toggle on-top'))
        print(fmt % ('z/Z', 'minimize window'))
        print(fmt % ('[0-4]', 'toggle font size'))
        return

    def run(self):
        service_name = 'org.gtk.clock'
        wm_class = StartupWMClass
        if self.args.fullscreen:
            service_name = 'org.gtk.clock.fullscreen'
            wm_class = StartupWMClassMax3
        elif self.args.maximize:
            service_name = 'org.gtk.clock.maximize'
            wm_class = StartupWMClassMax3
        elif self.args.screensaver:
            service_name = 'org.gtk.clock.screensaver'
            wm_class = StartupWMClassSS
        elif self.args.list_shortcuts:
            self.list_shortcuts()
            return
        elif self.args.list_colors:
            Color.list_colors()
            return

        mainappui = MainAppUI(
            service_name=service_name, cli_args=self.args, wm_class=wm_class
        )
        mainappui.run()
        return


class MainAppUI(Gtk.Application):

    def __init__(self, service_name=None, cli_args=None, wm_class=None):
        super().__init__(application_id=service_name)
        self.args = cli_args
        self.wm_class = wm_class
        self.main_win = None
        self.opt_screensaver = self.args.screensaver
        self.opt_fullscreen = self.args.fullscreen
        self.opt_maximize = self.args.maximize
        self.opt_ontop = self.args.ontop
        self.opt_second = not self.args.no_second
        self.opt_frame = self.args.frame
        self.opt_blank = False
        self.thread = None
        self.main_has_stopped = False

        self.max_width = 0

        return

    def do_activate(self):
        if not self.main_win:
            self.selfstart()
            return
        self.main_raise()

    def selfstart(self):
        self.sig_handler = self.Handler(self)
        self.main_win = Gtk.Window()
        self.main_win.connect('destroy', self.sig_handler.on_destroy)
        self.main_win.connect('key-press-event', self.sig_handler.on_keypress)
        self.main_win.connect(
            'button-press-event',
            self.sig_handler.on_button_press
        )
        self.interval = 0.1

        self.th_ev_stop = threading.Event()
        self.th_ev_stop.clear()
        self.th_ev_interval_reset = threading.Event()
        self.th_ev_interval_reset.clear()

        self.main_win.set_app_paintable(False)
        self.main_win.connect('draw', self.on_draw)
        self.main_win.resize(1, 1)

        self.set_fullscreen()
        self.set_screensaver()
        self.set_maximize()
        self.set_frame()
        self.set_ontop()

        # catch exception before run
        if not self.check_color():
            return False

        self.thread = threading.Thread(target=self.run_sched, args=())
        self.thread.daemon = True
        self.thread.start()

        self.main_win.set_wmclass(self.wm_class, self.wm_class)
        self.main_win.set_title(appname)
        self.main_win.set_gravity(Gdk.Gravity.CENTER)
        self.main_window_move_center()
        self.main_win.show_all()
        self.main_raise()

        Gtk.main()
        return True

    def main_raise(self):
        self.main_win.show()
        self.main_win.present()
        return

    def main_exit(self):
        '''
        There could be multiple invocation of this interface upon
        handling motion-notify-event, use `self.main_has_stopped` to
        track that.
        '''
        debug_log()
        if self.main_has_stopped:
            return True
        self.main_has_stopped = True
        if self.thread:
            # NOTE
            # call self.th_ev_stop.set() before wakeup
            # self.th_ev_interval_reset.
            self.th_ev_stop.set()
            self.th_ev_interval_reset.set()
            self.thread.join()
            self.thread = None
        self.tid_datetime = 0
        Gtk.main_quit()
        return True

    def run_sched(self):
        while True:
            # debug_log()
            self.th_ev_interval_reset.clear()
            if self.th_ev_stop.is_set():
                debug_log('stop event is set')
                break
            self.run_draw()
            self.set_interval()
            self.th_ev_interval_reset.wait(timeout=self.interval)
            # debug_log('woke up')
        debug_log('thread exit')
        return

    def check_color(self):
        unsupported_names = []
        for name in (self.args.fgcolor, self.args.bgcolor):
            if name and Color.get_rgb(name) is None:
                unsupported_names.append(name)

        if unsupported_names:
            print((
                'Unsupported color:\n    {}\nSupported color:'
            ).format(unsupported_names))
            Color.list_colors()
            return False

        return True

    @property
    def pango_layout(self):
        if not hasattr(self, '_pango_layout'):
            self._pango_layout = self.main_win.create_pango_layout()
        return self._pango_layout

    @property
    def time_fmt(self):
        if self.date_time_show_date:
            return self.ymd_fmt()
        else:
            return self.hms_fmt()

    def ymd_fmt(self):
        n = datetime.now()
        fmt = '{:02d}-{:02d}-{:02d}'.format((n.year % 100), n.month, n.day)
        return fmt

    def hms_fmt(self):
        n = datetime.now()
        if self.args.display12:
            fmt = '%d' % (n.hour if n.hour <= 12 else n.hour - 12)
        else:
            fmt = '%02d' % n.hour
        fmt += ':%02d' % n.minute
        if self.opt_second:
            fmt += ':%02d' % n.second
        return fmt

    @property
    def font_fmt(self):
        fmt = 'Times, Sans, Serif %s' % self.font_size
        fmt = 'Arial, Times, Sans, Serif %s' % self.font_size
        return fmt

    @property
    def font_size(self):
        if not hasattr(self, '_font_size'):
            self._font_size = 64
        return self._font_size

    @font_size.setter
    def font_size(self, ftsize):
        if hasattr(self, '_font_size') and self._font_size == ftsize:
            return
        self.max_width = 0
        self._font_size = ftsize
        self.interval_reset_needed = True
        self.th_ev_interval_reset.set()
        return

    @property
    def opt_builtin_scale(self):
        if not hasattr(self, '_opt_builtin_scale'):
            self._opt_builtin_scale = 1
            builtin0 = 32           # 8 * 4
            builtin1 = 64           # 8 * 4 * 2
            builtin2 = 128          # 8 * 4 * 4
            builtin3 = 240          # 8 * 5 * 6
            builtin4 = 480          # 8 * 5 * 12
            builtin = 64
            if self.args.builtin:
                self._opt_builtin_scale = 1
                self.font_size = builtin
            elif self.args.builtin0:
                self._opt_builtin_scale = 0
                self.font_size = builtin0
            elif self.args.builtin1:
                self._opt_builtin_scale = 1
                self.font_size = builtin1
            elif self.args.builtin2:
                self._opt_builtin_scale = 2
                self.font_size = builtin2
            elif self.args.builtin3:
                self._opt_builtin_scale = 3
                self.font_size = builtin3
            elif self.args.builtin4:
                self._opt_builtin_scale = 4
                self.font_size = builtin4
        return self._opt_builtin_scale

    @opt_builtin_scale.setter
    def opt_builtin_scale(self, scale):
        if (
                hasattr(self, '_opt_builtin_scale') and
                self._opt_builtin_scale == scale
        ):
            return
        builtin0 = 32           # 8 * 4
        builtin1 = 64           # 8 * 4 * 2
        builtin2 = 128          # 8 * 4 * 4
        builtin3 = 240          # 8 * 5 * 6
        builtin4 = 480          # 8 * 5 * 12
        builtin = 64
        if scale == 0:
            self._opt_builtin_scale = 0
            self.font_size = builtin0
        elif scale == 1:
            self._opt_builtin_scale = 1
            self.font_size = builtin1
        elif scale == 2:
            self._opt_builtin_scale = 2
            self.font_size = builtin2
        elif scale == 3:
            self._opt_builtin_scale = 3
            self.font_size = builtin3
        elif scale == 4:
            self._opt_builtin_scale = 4
            self.font_size = builtin4
        else:
            raise ValueError('invalid font scale: ' + scale)
        return

    @property
    def fg_rgba(self):
        if not hasattr(self, '_fg_rgba'):
            self._fg_rgba = self.get_color_fmt()[0]
        return self._fg_rgba

    @property
    def bg_rgba(self):
        if not hasattr(self, '_bg_rgba'):
            self._bg_rgba = self.get_color_fmt()[1]
        return self._bg_rgba

    @property
    def bg_border_rgba(self):
        if not hasattr(self, '_bg_border_rgba'):
            self._bg_border_rgba = Gdk.RGBA(0.1, 0, 1.0, 1.0)
        return self._bg_border_rgba

    def get_color_fmt(self):
        RGB = [0xff, 0xff, 0xff]
        RGB_INV = [0x00, 0x00, 0x00]

        fg = Color.get_rgb(self.args.fgcolor)
        bg = Color.get_rgb(self.args.bgcolor)
        if not fg and not bg:
            fg = RGB
            bg = RGB_INV
        elif not fg:
            fg = Color.get_rgb(self.args.bgcolor, invert=True)
        elif not bg:
            bg = Color.get_rgb(self.args.fgcolor, invert=True)

        fg_rgba = Gdk.RGBA(fg[0]/255.0, fg[1]/255.0, fg[2]/255.0, 1.0)
        bg_rgba = Gdk.RGBA(bg[0]/255.0, bg[1]/255.0, bg[2]/255.0, 1.0)

        return (fg_rgba, bg_rgba)

    def run_draw(self):
        '''
        According to GLib Reference Manual:

            guint g_idle_add (GSourceFunc function, gpointer data);

        If the function returns FALSE it is automatically removed
        from the list of event sources and will not be called again.
        '''
        GLib.idle_add(self.on_draw_nocr, args=())

        # Don't use gtk_widget_queue_draw() because the 'draw' signal
        # will stuck after some period.
        # self.main_win.queue_draw()

    def on_draw_nocr(self):
        method1 = True
        method1 = False
        if method1:
            surface = self.main_win.get_window()
            cr = Gdk.cairo_create(surface)
            self.on_draw(widget=self.main_win, cr=cr)
        else:
            # not method1:
            r = Gdk.Rectangle()
            r.width = self.main_win.get_allocated_width()
            r.height = self.main_win.get_allocated_height()
            w = self.main_win.get_window()
            if w:
                w.invalidate_rect(r, False)

        # return 'False', see comment of run_draw()
        return False

    def on_draw(self, widget=None, cr=None):
        # debug_log()
        self.pango_layout.set_text(self.time_fmt, -1)
        desc = Pango.font_description_from_string(self.font_fmt)
        self.pango_layout.set_font_description(desc)

        (lw, lh) = self.pango_layout.get_pixel_size()

        if lw > self.max_width:
            self.max_width = lw
            debug_log((lw, self.max_width))

        lw = self.max_width

        # self.main_win.set_size_request(
        #     lw * 1.1 if self.opt_second else
        #     lw * 1.2,
        #     lh
        # )
        self.main_win.resize(
            lw * 1.1 if self.opt_second else
            lw * 1.2,
            lh
        )

        width = widget.get_allocated_width()
        height = widget.get_allocated_height()

        # bg
        cr.save()
        draw_border = (
            True if self.opt_ontop and not self.opt_screensaver else
            False
        )
        cr.rectangle(0, 0, width, height)
        Gdk.cairo_set_source_rgba(cr, self.bg_rgba)
        cr.fill()
        if draw_border:
            cr.set_line_width(1.5)
            cr.rectangle(0, 0, width, height)
            Gdk.cairo_set_source_rgba(cr, self.bg_border_rgba)
            cr.stroke()
        cr.restore()

        if self.opt_blank:
            return True

        # text
        cr.save()
        x = (width - lw) / 2
        y = (height - lh) / 2
        cr.move_to(x, y)
        Gdk.cairo_set_source_rgba(cr, self.fg_rgba)
        PangoCairo.show_layout(cr, self.pango_layout)
        if self.font_border:
            cr.rectangle(x, y, lw, lh)
            cr.stroke()
        cr.restore()

        return True

    def main_window_move_center(self):
        screen = self.main_win.get_screen()
        sw = screen.get_width()
        sh = screen.get_height()
        x = (sw) / 2
        y = (sh) / 2
        self.main_win.move(x, y)
        return

    def set_frame(self):
        if self.opt_frame == 'none':
            self.main_win.set_decorated(False)
        elif self.opt_frame == 'always':
            self.main_win.set_decorated(True)
        elif self.opt_frame == 'auto':
            self.main_win.set_decorated(not self.main_win.is_maximized())
        return

    def toggle_decoration(self):
        self.main_win.set_decorated(not self.main_win.get_decorated())
        return

    def set_minimize(self):
        self.main_win.iconify()
        return

    def set_maximize(self):
        if self.opt_maximize:
            self.set_adaptive_font()
            self.main_win.maximize()
            self.opt_builtin_scale = 3
            self.opt_second = True
        else:
            self.main_win.unmaximize()
            self.main_window_move_center()
        return

    def toggle_maximize(self):
        self.opt_maximize = not self.opt_maximize
        self.set_maximize()
        return

    def toggle_fullscreen(self):
        self.opt_fullscreen = not self.opt_fullscreen
        self.opt_maximize = True
        self.set_fullscreen()
        self.set_maximize()
        self.set_frame()
        self.run_draw()
        return

    def leave_fullscreen(self):
        if not self.opt_fullscreen:
            return
        self.opt_fullscreen = False
        self.opt_maximize = True
        self.set_fullscreen()
        self.set_maximize()
        self.set_frame()
        self.run_draw()
        return

    def set_fullscreen(self):
        if self.opt_fullscreen:
            self.set_adaptive_font()
            self.main_win.fullscreen()
            w = self.main_win.get_window()
            if w:
                w.set_cursor(Gdk.Cursor(Gdk.CursorType.BLANK_CURSOR))
        else:
            self.main_win.unfullscreen()
            w = self.main_win.get_window()
            if w:
                w.set_cursor(None)
        return

    def set_adaptive_font(self):
        todo()
        # TODO
        return

    def set_screensaver(self):
        if not self.args.screensaver:
            return
        debug_log()

        self.opt_ontop = True
        self.opt_builtin_scale = 3
        self.opt_second = True
        self.main_win.fullscreen()
        self.main_win.disconnect_by_func(self.sig_handler.on_keypress)
        self.main_win.connect('key-press-event', self.sig_handler.on_destroy)
        self.main_win.connect('touch-event', self.sig_handler.on_destroy)

        self.main_win.add_events(Gdk.EventMask.POINTER_MOTION_MASK)
        self.main_win.connect(
            'motion-notify-event', self.sig_handler.on_destroy
        )
        self.main_win.connect(
            'realize', self.sig_handler.screensaver_hide_cursor
        )
        return True

    def handle_hide_cursor(self):
        if not self.main_win.get_realized():
            return False
        w = self.main_win.get_window()
        if w:
            w.set_cursor(Gdk.Cursor(Gdk.CursorType.BLANK_CURSOR))
        return

    def set_ontop(self):
        if self.opt_ontop:
            self.main_win.set_keep_above(True)
            debug_log('set_keep_above(True)')
        else:
            self.main_win.set_keep_above(False)
            debug_log('set_keep_above(False)')
        self.main_win.queue_draw()
        return

    def toggle_ontop(self):
        self.opt_ontop = not self.opt_ontop
        self.set_ontop()
        return

    def set_interval(self):
        now = datetime.now()
        # seconds
        total_seconds = 0
        if not self.opt_second:
            total_seconds = 59.0 - now.second
        # microseconds
        max = 0.9999
        min = 0.02
        residue = 1.0 - (now.microsecond / 1000000)
        if residue >= max:
            residue = max
        elif residue <= min:
            residue = min

        self.interval = total_seconds + residue
        # debug_log(self.interval)
        return

    def toggle_second(self):
        self.max_width = 0
        self.opt_second = not self.opt_second
        self.interval_reset_needed = True
        self.th_ev_interval_reset.set()
        self.run_draw()
        return

    def toggle_blank(self):
        self.opt_blank = not self.opt_blank
        self.run_draw()
        return

    def toggle_transparency(self):
        opacity = self.main_win.get_opacity()
        self.main_win.set_opacity(
            0.6 if opacity > 0.7 else
            0.3 if opacity > 0.4 else
            1
        )
        debug_log('opacity: {:.2f}'.format(self.main_win.get_opacity()))
        return

    def toggle_font_border(self):
        self.font_border = not self.font_border
        self.run_draw()
        return

    @property
    def font_border(self):
        if not hasattr(self, '_font_border'):
            self._font_border = False
        return self._font_border

    @font_border.setter
    def font_border(self, border):
        self._font_border = True if border else False
        debug_log(self._font_border)
        return

    @property
    def date_time_show_date(self):
        if not hasattr(self, '_date_time_show_date'):
            self._date_time_show_date = False
        return self._date_time_show_date

    @date_time_show_date.setter
    def date_time_show_date(self, show_date=False):
        self._date_time_show_date = show_date
        return

    @property
    def tid_datetime(self):
        if not hasattr(self, '_tid_datetime'):
            self._tid_datetime = 0
        return self._tid_datetime

    @tid_datetime.setter
    def tid_datetime(self, tid):
        if self.tid_datetime > 0:
            debug_log('remove tid_datetime')
            GLib.source_remove(self.tid_datetime)
        self._tid_datetime = tid
        return

    def toggle_date_time(self):
        debug_log()
        self.run_draw()
        self.date_time_show_date = True
        self.tid_datetime = GLib.timeout_add(
            TOGGLE_DATE_TIME_DURATION,
            self.toggle_date_time_done
        )
        return

    def toggle_date_time_done(self):
        debug_log()
        self.tid_datetime = 0
        self.run_draw()
        self.date_time_show_date = False
        return False

    class Handler:

        def __init__(self, parent=None):
            self.main_app = parent
            return

        def on_destroy(self, *args):
            debug_log()
            self.main_app.main_exit()
            return True

        def screensaver_hide_cursor(self, widget):
            self.main_app.handle_hide_cursor()
            return True

        def on_button_press(self, widget, ev):
            self.main_app.toggle_date_time()
            return True

        def on_keypress(self, widget, event, data=None):
            kv = event.keyval
            msg = 'pressed {:c} (0x{:x})'.format(kv, kv)
            if kv in (Gdk.KEY_Q, Gdk.KEY_q, Gdk.KEY_W, Gdk.KEY_w):
                info = 'quit'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.main_exit()
                return True
            elif kv in (Gdk.KEY_f, Gdk.KEY_F):
                info = 'toggle fullscreen'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_fullscreen()
                return True
            elif kv == Gdk.KEY_Escape:
                self.main_app.leave_fullscreen()
                return True
            elif kv in (Gdk.KEY_x, Gdk.KEY_X):
                info = 'toggle maximize'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_maximize()
                return True
            elif kv in (Gdk.KEY_s, Gdk.KEY_S, Gdk.KEY_space):
                info = 'toggle second'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_second()
                return True
            elif kv in (Gdk.KEY_t, Gdk.KEY_T):
                info = 'toggle ontop'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_ontop()
                return True
            elif kv in (Gdk.KEY_d, Gdk.KEY_D):
                info = 'toggle decoration'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_decoration()
                return True
            elif kv in (Gdk.KEY_o, Gdk.KEY_O):
                info = 'toggle opacity'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_transparency()
                return True
            elif kv in (Gdk.KEY_b, Gdk.KEY_B):
                info = 'toggle blank'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_blank()
                return True
            elif kv in (Gdk.KEY_v, Gdk.KEY_V):
                info = 'toggle font border'
                debug_log('{}: {}'.format(msg, info))
                self.main_app.toggle_font_border()
                return True
            elif kv in (Gdk.KEY_z, Gdk.KEY_Z):
                info = ('minimize window')
                debug_log('{}: {}'.format(msg, info))
                self.main_app.set_minimize()
                return True
            elif kv in (0x30, 0x31, 0x32, 0x33, 0x34):
                self.main_app.opt_builtin_scale = (kv - 0x30)
                return True

            # Capture any key stroke.
            return True
